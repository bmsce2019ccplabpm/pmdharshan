#include <math.h>
#include <stdio.h>

float distance (float x1,float y1,float x2,float y2)
{
    float d = sqrt((pow(x2-x1,2))-(pow(y2-y1,2)));
    return d;
}

int main()
{
    float x1,y1,x2,y2,d;
    printf ("Enter x1, y1, x2, y2 :\n");
    scanf ("%f %f %f %f",&x1,&y1,&x2,&y2);
    d = distance (x1,y1,x2,y2);
    printf ("distance between %f,%f & %f,%f is %f\n",x1,y1,x2,y2,d);
    return 0;
}  