#include <stdio.h>
int main()
{
    struct DOJ
    {
        int day;
        int month;
        int year;
    };
    struct data
    {
        char name[20];
        int id;
        int salary;
        struct DOJ date;
    };
    
    struct data e;
    printf("Enter the name:");
    gets(e.name);
    printf("Enter DOJ:");
    scanf("%d %d %d",&e.date.day,&e.date.month,&e.date.year);
    printf("Enter id:");
    scanf("%d",&e.id);
    printf("Enter salary:");
    scanf("%d",&e.salary);
    printf("*******EMPLOYEE DETAIL*******");
    printf("\nNAME:");
    puts(e.name);
    printf("DOJ:%d %d %d",e.date.day,e.date.month,e.date.year);
    printf("\nID:%d",e.id);
    printf("\nSALARY:%d",e.salary);
    return 0;
} 