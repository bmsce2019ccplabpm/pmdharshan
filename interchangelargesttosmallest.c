#include <stdio.h>
int main()
{
    int i,n,small,small_pos,large,large_pos,temp;
    printf("Enter number of elements:");
    scanf("%d",&n);
    int arr[n];
    for(i=0;i<n;i++)
    {
        printf("Enter the value of element:");
        scanf("%d",&arr[i]);
    }
    small=arr[0];
    small_pos=0;
    for(i=1;i<n;i++)
    {
        if(arr[i]<small)
        {
            small=arr[i];
            small_pos=i;
        }
    }
    large=arr[0];
    large_pos=0;
    for(i=1;i<n;i++)
    {
        if(arr[i]>large)
        {
            large=arr[i];
            large_pos=i;
        }
    }
    printf("The values before interchanging of largest and smallest number is %d & %d respectively.\n",arr[large_pos],arr[small_pos]);
    
    temp=arr[small_pos];
    arr[small_pos]=arr[large_pos];
    arr[large_pos]=temp;
    printf("The new values after interchanging largest and smallest number is %d & %d respectively.\n",arr[large_pos],arr[small_pos]);
    printf("The new array is:");
    for(i=0;i<n;i++)
        printf("%d",arr[i]);
    return 0;
}   