#include <stdio.h>
int main()
{
    int a[5][3],i,j,max_marks;
    for(i=0;i<5;i++)
    {
        printf("\nEnter the marks of student %d",i);
        for(j=0;j<3;j++)
        {
            printf("\nMarks in subject a[%d][%d]=",i,j);
            scanf("%d",&a[i][j]);
        }
    }
    for(j=0;j<3;j++)
    {
        max_marks=a[0][j];
        for(i=1;i<5;i++)
        {
            if(a[i][j]>max_marks)
            max_marks=a[i][j];
        }
        printf("\nThe highest marks in %d subject = %d",j,max_marks);
    }
    return 0;
}   